package com.example.demo.service.impl;

import com.example.demo.service.EncryptDecryptService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Service
public class EncryptDecryptServiceImpl implements EncryptDecryptService {

    @Value("${fileName}")
    private String fileName;

    @Override
    public String getSecretKey(){
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String secretKey = bufferedReader.readLine();
            return secretKey == null ? "" : secretKey;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public TextEncryptor getTextEncryptor(String secretKey) {
        String generateKey = KeyGenerators.string().generateKey();
        return Encryptors.text(secretKey, generateKey);
    }
}
