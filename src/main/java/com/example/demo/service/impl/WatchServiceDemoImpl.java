package com.example.demo.service.impl;

import com.example.demo.service.EncryptDecryptService;
import com.example.demo.service.WatchServiceDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;

@Service
public class WatchServiceDemoImpl implements WatchServiceDemo {

    private WatchService watchService;

    private EncryptDecryptService encryptDecryptService;

    private TextEncryptor textEncryptor;

    @Value("${folderName}")
    private String folderName;


    @Autowired
    public WatchServiceDemoImpl(EncryptDecryptService encryptDecryptService) {
        try {
            this.watchService = FileSystems.getDefault().newWatchService();
            this.encryptDecryptService = encryptDecryptService;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @PostConstruct
    public void fileRegistration() {
        Thread thread = new Thread(() -> {
            if (watchService != null) {
                try {
                    String filePath = new File(folderName).getPath();
                    Path path = Paths.get(filePath);

                    path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                while (true) {
                    WatchKey key;
                    try {
                        key = watchService.take();
                    } catch (InterruptedException ex) {
                        return;
                    }
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();
                        if (kind.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                            setTextEncryptor();
                        }
                    }
                    key.reset();
                }
            }
        });

        thread.start();
    }

    @Override
    public TextEncryptor getTextEncryptor() {
        return textEncryptor;
    }


    @PostConstruct
    private synchronized void setTextEncryptor(){
        String secretKey = encryptDecryptService.getSecretKey();
        textEncryptor = encryptDecryptService.getTextEncryptor(secretKey);
    }

}
