package com.example.demo.service;


import org.springframework.security.crypto.encrypt.TextEncryptor;

public interface WatchServiceDemo {

    TextEncryptor getTextEncryptor();
}
