package com.example.demo.service;

import org.springframework.security.crypto.encrypt.TextEncryptor;

public interface EncryptDecryptService {

    String getSecretKey();

    TextEncryptor getTextEncryptor(String secretKey);
}
