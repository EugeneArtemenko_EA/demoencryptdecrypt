package com.example.demo.controller;


import com.example.demo.service.WatchServiceDemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AESEncryptDecryptRestController {

    private WatchServiceDemo watchServiceDemo;

    private Logger logger = LoggerFactory.getLogger(AESEncryptDecryptRestController.class);

    @Autowired
    public AESEncryptDecryptRestController(WatchServiceDemo watchServiceDemo){
        this.watchServiceDemo = watchServiceDemo;
    }

    @RequestMapping(value = "/app/encrypt", method = RequestMethod.POST)
    public ResponseEntity<?> encryptString(@RequestBody String stringForEncrypt) {
        showTextEncryptorHashCode();
        String response = watchServiceDemo.getTextEncryptor().encrypt(stringForEncrypt);
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/app/decrypt", method = RequestMethod.POST)
    public ResponseEntity<?> decryptString(@RequestBody String stringForDecrypt) {
        showTextEncryptorHashCode();
        String response = watchServiceDemo.getTextEncryptor().decrypt(stringForDecrypt);
        return ResponseEntity.ok(response);
    }

    private void showTextEncryptorHashCode(){
        int hashCode = watchServiceDemo.getTextEncryptor().hashCode();
        logger.warn("########################## TextEncryptor hashCode = " + hashCode + " #############################");
    }
}
